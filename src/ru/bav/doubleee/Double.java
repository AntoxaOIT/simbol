package ru.bav.doubleee;

import java.util.Scanner;

/**
 * Created by антон on 26.01.2017.
 *
 * @author Barinov 15OIT18.
 */
public class Double {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        double number = scanner.nextDouble();
        System.out.println(number + (number % 1 == 0 ? " - Число является целым" : " -Число является не целым"));
    }

}


