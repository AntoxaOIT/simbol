package ru.bav.timeofdaysmin;

import java.util.Scanner;

/**
 * Created by антон on 31.01.2017.
 */
public class Time {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        int days;
        System.out.println("Введите количество дней");
        days = scanner.nextInt();
        System.out.println("Дней - " + days + ", " + "Часов - " + days*24 + ", " + "Минут - " + days*24*60+", " + "Секунд - " + days*24*60*60 + ".");
    }

}
