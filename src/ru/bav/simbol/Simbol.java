package ru.bav.simbol;

import java.util.Scanner;

/**
 * Класс ,который считает символы пока не встретится точка ,учитывая пробелы.
 *
 * @author Barinov 15OIT18.
 */
public class Simbol {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите строку : ");
        String text = scanner.nextLine();
        System.out.println("Количество символов до точки = " + controlofsimbol(text));
        System.out.println("Количество пробелов до точки = "+spaces(text));
    }

    /**
     * Метод который считает количество элементов строки до точки.
     *
     * @param stroka введенная строка с клавиатуры.
     */
    public static int controlofsimbol(String stroka) {

        int a = stroka.indexOf(".");

        return a;
    }

    public static int spaces(String text) {
        int k = 0;
        for (int i = 0; i < text.length(); i++) {

            if (text.charAt(i) == ' ') {
                k++;
            }
        }
        return k;
    }

}
