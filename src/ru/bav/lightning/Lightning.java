package ru.bav.lightning;

import java.util.Scanner;

public class Lightning {
    /**
     * Класс для расчета расстояния до места удара молнии.
     *
     * @author Barinov 15OIT18
     */

        static Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) {
            double speed = 1234.8;
            double way;
            double interval;

            System.out.println("Введите время после вспышки(в секудах):");
            interval = scanner.nextDouble();
            way = interval * speed;
            System.out.println("Расстояние до молнии = " + way + " км.");

        }
    }

