package ru.bav.curs;

import java.util.Scanner;

/**
 * Created by антон on 26.01.2017.
 */
public class Curs {
    /**
     * Класс для демонстрации работоспособности  метода для перевода рублей в евро по заданному курсу.
     *
     * @author Barinov 15OIT18.
     */
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Введите курс евро : ");
        double curs = scanner.nextDouble();
        System.out.println("Введите количество рублей, которые хотите перевести в евро : ");
        int rub = scanner.nextInt();
        trade(rub, curs);

        System.out.println(rub + " рублей" + " = " + String.format("%.2f", trade(rub, curs)) + " евро.");
    }

    /**
     * Метод для перевода рублей в евро по заданному курсу
     *
     * @param rub  количество рублей
     * @param curs курс евро
     * @return количество рублей в евро
     */
    public static double trade(int rub, double curs) {
        return rub / curs;
    }
}

