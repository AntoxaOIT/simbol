package ru.bav.Tabl;

import java.util.Scanner;

/**
 * Добавлен класс таблица умнжения
 *
 * Created by антон on 26.01.2017.
 * @author Barinov 15OIT18.
 */
public class Tabl {
    /**
     * Класс для того, чтобы вывести таблицу умножения введенного пользователем числа с клавиатуры.
     *
     * @author Barinov 15OIT18
     */

        static Scanner scanner = new Scanner(System.in);

        public static void main(String[] args) {

            System.out.println("Введите число : ");
            int number = scanner.nextInt();
            writetabl(number);
        }

        /**
         * Метод для вывода таблицы умножения для введеного пользователем числа.
         *
         * @param number число,введенное пользователем
         */
        private static void writetabl(int number) {
            for (int i = 1; i <= 10; i++) {
                System.out.println(number + " * " + i + " = " + i * number);
            }
        }
}
